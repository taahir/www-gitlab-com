---
layout: handbook-page-toc
title: "Twitter response workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

### Handles for social channels

| HANDLE | RESPOND FROM | GUIDELINES |
| - | - | - |
| [@GitLabStatus](https://twitter.com/GitLabStatus) | Zendesk | Post service updates |
| [@GitLab](https://twitter.com/GitLabs) | Zendesk | Respond to mentions and questions |

- The [@GitLabStatus](https://twitter.com/GitLabStatus) account should only be used to give updates on the availability of [GitLab.com](https://gitlab.com) and to follow up on users reporting that [GitLab.com](https://gitlab.com) is unavailable or responding to a previous availability update on [@GitLabStatus](https://twitter.com/GitLabStatus).
- Only the infrastructure team should be posting updates on [@GitLabStatus](https://twitter.com/GitLabStatus). There is a [defined process](/handbook/engineering/infrastructure/incident-management/) for this describing who should do this, how and what channels should be alerted.

## Workflow

- Community Operations should use their best judgment and reply to tweets, following the [social media guidelines](/handbook/marketing/social-media-guidelines/), in these two cases: 

1. To route users to the forum if they need help with a technical issue or inquiry
1. Tweet content when asked explicitly to do so by the Social Media Team or the Communications Team

Community Operations uses Zendesk via Zapier to pull in all mentions of GitLab on Twitter. When resolving Twitter tickets in Zendesk you should:

1. Look for technical questions and send them to the forum via twitter response, via Zendesk
1. Look for opportunities for re-tweets that may interest the Social Media Team
1. Look for patterns in negative sentiment indicating a bad community response to an expected of unexpected GitLab change
1. Look for opportunites to engage the Developer Evengelists, or other experts, via the [Involving Experts Workflow](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts/)

## Best practices


### Tweets regarding GitLab downtime

<<<<<<< HEAD
Check if we're experiencing any issues with our system on [GitLab System Status](https://status.gitlab.com/) page or if there were any official updates on our [Twitter GitLab.com Status](https://twitter.com/gitlabstatus/) profile. If you find anything that might be related, please follow up with the user and point them toward the status page.

If the incident is serious enough, consider creating a Zendesk tag for related tweets in order to review this data later in potential retrospectives.
=======
Check if we're experiencing any issues with our system on [GitLab System Status](https://status.gitlab.com/) page or if there were any official updates on our [Twitter GitLab.com Status](https://twitter.com/gitlabstatus/) profile. If you find anything that might be related, please follow up with the user forwarding that link and asking if they are still experiencing issues.
>>>>>>> d2033355c45add7c289d0e5ff3d6840c6c9ec3b8


### Support Related Questions

Often, users will tweet their support related questions to the main @gitlab account. Solving support related questions via twitter is not the preferred method, as the character limit is difficult to work with, and resolutions could be lost for future use.

<<<<<<< HEAD
Instead, please direct users to post their question on the [GitLab Forum](https://forum.gitlab.com/).
=======
The [involving experts workflow](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts/) can sometimes be an effective way to response to users. In addition, advocates should consider utilizing the GitLab support zendesk instance to find sharable resources, or direct users to post their question on the [GitLab Forum](https://forum.gitlab.com/)

#### Using GitLab Support Zendesk

*  All advocates should request access as a ['Light Agent' on the Support Zendesk Instance](/handbook/support/internal-support/#viewing-support-tickets) during onboarding.
*  From the support related tweet, pull out keywords that best describe the user's problem. Try to keep these to short phrases to avoid limiting your Zendesk search.
*  In the support Zendesk instance, use the search function to sort through all support tickets with these key words.
*  To find new information about bug reports, status issues, or urgent matters, click on the 'Updated' column to sort the newest tickets to the top of the results.
*  To find solved tickets that address common issues GitLab users face, but that are not restricted to time, add `status:solved` to your search.
*  Skim through the first few tickets to see if you can find a user who has submitted a similar question. In this step, the advocates needs to do a little bit of searching to find related issues, solutions, or resources to share back with the user.
*  If the advocate can find an issue link or resource to share in a short tweet, they should respond from the @gitlab account via Zendesk.
*  If the question involves a detailed response, they should use the [steps to direct users to the forum](#directing-users-to-the-forum).
>>>>>>> d2033355c45add7c289d0e5ff3d6840c6c9ec3b8

#### Directing Users to the Forum

Try to find a related topic in the forum (preferably one that is solved!) and share a direct link. If no similar topic exists, share the link to the most appropriate forum category.

Consider using language like this in your tweet to best encourage forum use:
* `Posting in the forum allows the GitLab team and the wider community to help find solutions for your needs, create issues for long-term solutions, and update our documentation.`
* `In order to troubleshoot your issue, please post your question in our forum at https://forum.gitlab.com! This way you'll have the whole power of the community to help.`
* `It's likely these community experts will be able to help: (link to forum topic) since they have worked through something similar before. Post your question and we'll check in on it!`

<<<<<<< HEAD
=======
Related experts can be found in [#support_gitlab-com](https://gitlab.slack.com/messages/support_gitlab-com), [#support_self-managed](https://gitlab.slack.com/messages/support_self-managed), and [#support_escalations](https://gitlab.slack.com/messages/CBVAE1L48/) in Slack.

#### Escalate a Support Ticket

Users will often tweet to bring attention to a ticket they submitted to support. Advocates can respond by

* Asking the user for their ticket number and searching the Support Zendesk instance to check on the status
* If the ticket has not been addressed, [follow the Slack process to raise attention to a support ticket](/handbook/support/internal-support/#i-want-to-escalate-a-ticket)
* Respond back to the user to let them know you've reached out to support, and that the team is working on their request.


### Usage of Likes

Use "Likes" on Twitter for promoting positive feedback about our product, since we direct users there when we want to show that people really love the product. Avoid using it for anything else.

### Retweeting

Advocates shouldn't retweet anything from the official GitLab Twitter accounts. If you see something that should be retweeted, paste the tweet in [`#social_media_action` Slack Channel](https://gitlab.slack.com/archives/C4UGNMF9A) for the social team to review.

### Direct Messages

We have direct messages disabled in our Twitter accounts, but they can be used if we first send a direct message to a user. This should only be used when the user needs to communicate with us privately (e.g. to give a mailing address).

>>>>>>> d2033355c45add7c289d0e5ff3d6840c6c9ec3b8
## Automation

Tweets that mention [@GitLab](https://twitter.com/GitLab), or [@GitLabStatus](https://twitter.com/GitLabStatus) will create a ticket in Zendesk, and show up in the "Twitter" view.

If a tweet is responded to from TweetDeck, this risks duplicate responses. Responding from Zendesk also enables us to track our response times vs. [our internal SLA](/handbook/support/#sla).

## Tweetdeck

Tweetdeck can be used to delete tweets if something is sent accidentally from Zendesk. If you send an accidental tweet from the @GitLab handle, please notify the Social Media team asap via the #social_media_action slack channel.

