---
layout: handbook-page-toc
title: "Effective Objection Handling Practice"
---

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

# Effective Objection Handling Practice
{:.no_toc}

Effective Objection handling skill is critical to becoming a trusted advisor in our respective sales/customer success roles. Objection handling means responding to the customer in a way that changes their mind or alleviate their concerns. Objections typically fall into these **categories**(http://changingminds.org/disciplines/sales/objection/objection_types.htm)

# Objection Handling Process

The Objection Handling Process can be broken down into following steps:

1. Listen

Active listening is a key part of effective objection handling. Use methods like nodding and physically showing interest.
This is our chance to do more discovery & learn as much as about the customer as possible.

2. Question

Ask **questions**(http://changingminds.org/techniques/questioning/questioning.htm) if it makes sense, generally we find open ended questions are the best way to retrieve information. This not only shows you are interested in them, but it also gives you more information with which to make the sale. 
As you question them, watch carefully for body language that gives you more information about what they are thinking and feeling.
You might also use the "tip the bucket technique" to see if they more objections that can be answered.

3. Think

Take a pause to think & figure out which **technique**(http://changingminds.org/disciplines/sales/objection/objection_handling.htm) is the best for their objection. You will see that these techniques will become second nature with practice.

4. Handle

Handle the objection in an appropriate time frame using the techniques above or however way you like.
You are under no obligation to try and force-fit a method where it is unlikely to work.

5. Check

Finally, check to find out whether your objection-handling worked! Ask if you have answered their question. Ask if there are any more concerns. As necessary, handle outstanding objections.

Then go for the close using any of these **closing techniques**(http://changingminds.org/disciplines/sales/closing/closing_techniques.htm).

# Objection Handling Techniques That Can Help

**LACE**(http://changingminds.org/disciplines/sales/objection/objection_lace.htm)
**Boomerang**(http://changingminds.org/disciplines/sales/objection/objection_boomerang.htm): By law of association, if you are stating what they stated to you previously then you have convinced the customer.
**Objection Chunking**: Taking a higher or lower viewpoint.
**Conditional Close**: Make closure a condition for resolving their objection.
**Curiosity**: Ask more questions
**Deflection: Avoid responding to objection, just letting it pass.
**Fallback: Step back and remove reason to object.
**Fear, Uncertainty and Doubt (FUD): A negative but effective ploy.
**Feel, felt, found: A classic way of moving them.
**Humor: Respond with humor rather than frustration.
**Justification: Say how reasonable the objection is.
**LAARC: Listen, Acknowledge, Assess, Respond, Confirm.
**LAIR: Listen, Acknowledge, Identify objection, Reverse it.
**Objection Writing: Write down and cross out objections.
**Pre-empting: Handle them before they happen.
**Pushback: Object to their objection.
**Reframing: Change their cognitive frame.
**Renaming: Change the words to change the meaning.
**Reprioritize: So ones you can't handle are lower.
**Writing: Write down objections then cross them off as you handle them.


## Objection Handling Role Play Exercise


## Practice Objection Handling in team meeeting by using the framework below

## Objection Handling Workshops

## More resources




## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


